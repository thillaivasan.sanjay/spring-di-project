package com.hexaware.spring.di;

import org.springframework.stereotype.Component;

@Component
public abstract class BankAccount {
    private long accountId;
    private String accountName;
    private double accountBalance;

    public long getAccountId() {
        return accountId;
    }



    public String getAccountName() {
        return accountName;
    }

    public void setAccountName(String accountName) {
        this.accountName = accountName;
    }

    public double getAccountBalance() {
        return accountBalance;
    }

    public void setAccountBalance(double accountBalance) {
        this.accountBalance = accountBalance;
    }

    public BankAccount(long accountId, String accountName) {
        this.accountId = accountId;
        this.accountName = accountName;
    }
}