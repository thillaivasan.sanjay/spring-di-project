package com.hexaware.spring.di;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.AbstractApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class CommunteClient {
    public static void main(String[] args) {
        ApplicationContext applicationContext = new ClassPathXmlApplicationContext("application-context.xml");
        /*Passenger passenger = applicationContext.getBean("passenger", Passenger.class);
        passenger.commute("Malleshwaram", "Peenya");

        System.out.println("Price per Km "+ passenger.getCar().getPricePerKm());

        passenger.getAddressList().forEach( address -> System.out.println(address.getZipCode()));
        */
        BankAccount savingsAccount = applicationContext.getBean("savingsAccount", SavingsAccount.class);
        ((SavingsAccount)savingsAccount).deposit(2000);
        System.out.println(savingsAccount.getAccountBalance());
        ((AbstractApplicationContext)applicationContext).registerShutdownHook();
    }
}