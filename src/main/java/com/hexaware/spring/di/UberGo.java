package com.hexaware.spring.di;

import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Component;

@Component
@Primary
public class UberGo implements Car {

    private double pricePerKm;

    public void setPricePerKm(double pricePerKm) {
        this.pricePerKm = pricePerKm;
    }

    public double getPricePerKm(){
        return this.pricePerKm;
    }

    public void commute(String source, String destination) {
        System.out.println("Driving from "+ source+ " till "+destination + " with Uber Go");
    }
}