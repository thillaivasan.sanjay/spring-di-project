package com.hexaware.spring.di;

import org.springframework.stereotype.Component;

@Component
public class UberXL implements Car {
    private double pricePerKm;

    public void commute(String source, String destination) {
        System.out.println("Travelling from source "+ source+ " to "+ destination + " witj Uber XL");
    }

    public void setPricePerKm(double pricePerKm) {
        this.pricePerKm = pricePerKm;
    }

    public double getPricePerKm() {
        return this.pricePerKm;
    }
}